const waterTaxModel=require("../schema/waterTax.schema");
const BaseService=require("@baapcompany/core-api/services/base.service");
class waterTaxService extends BaseService{
    constructor(dbModel, entityName){
        super(dbModel,entityName);
    }
    grtAllDataByGroupId(waterTaxId,criteria){
        const query={
            waterTaxId:waterTaxId,
        }
if(criteria.name)query.name=new RegExp(criteria.name ,"i");
return this.preparePaginationAndReturnData(query,criteria)
    }
    
async deleteAllByCriteria(criteria){
    try{
        const deleteResult=await waterTaxModel.deleteMany(criteria).exec()
   if (deleteResult.deleteCount === 0){
    return{message:"No find this filed"};
    }
    return{message:"All taxes are delete successful"}
}catch(error){
throw error;
}}
async getAllDataByTaxationPeriod(taxationPeriod, criteria) {
        const query = {
            taxationPeriod: taxationPeriod,
        };
        if (criteria.taxationPeriod) query.taxationPeriod = criteria.taxationPeriod

        return this.preparePaginationAndReturnData(query, criteria)
    }
}
module.exports=new waterTaxService(waterTaxModel,"watertax");