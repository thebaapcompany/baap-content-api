const BaseService = require("@baapcompany/core-api/services/base.service");
const languageModel = require("../schema/langauge.schema");

class languageService extends BaseService { 
    constructor(dbModel, entityName) {
        super(dbModel, entityName);
    }
    getAllDataByGroupId(groupId, criteria) {
        const query = {
          groupId: groupId,
        };
       return this.preparePaginationAndReturnData(query, criteria);
      }

    
    async findByCode(code) { // Corrected the method name to findByCode (camelCase)
        return this.execute(() => {
            return this.model.findOne({ code: code });
        });
    }

    async findByName(name) { // Corrected the method name to findByCode (camelCase)
        return this.execute(() => {
            return this.model.findOne({ name: name });
        });
    }
    async selectByApp(appId) { // Corrected the method name to findByCode (camelCase)
        return this.execute(() => {
            return this.model.findOne({ appId: appId });
        });
     }
    //  async deleteCustByIds(groupId, names) {
    //     try {
    //         const query = { groupId: groupId };
    
    //         if (names && names.length > 0) {
    //             query.name = { $in: names };
    //         }
    
    //         const result = await languageModel.deleteMany(query);
    //         return result;
    //     } catch (error) {
    //         console.error(error);
    //         throw { status: 500, error: 'Internal Server Error' };
    //     }
    // }
    async deleteDataById(groupId, langaugeId) {
        try {
            const updatedServicerequestData = await ServicerequestModel.deleteMany(
                {
                    groupId: groupId,
                    langaugeId: { $in: langaugeId },
                },
                { $set: { deleted: true } }
            );

            return updatedServicerequestData;
        } catch (error) {
            throw error;
        }
    }
    async getDataByGroupIdAndMemberId(groupId, appId, appName, moduleName, key, groupName, Name, page, size) {
        try {
            let query = { groupId };

            if (appId) {
                query.appId = parseInt(appId);
            }
            if (moduleName) {
                query.moduleName = moduleName;
            }
            if (key) {
                query.key = key;
            }
            if (appName) {
                query.appName = appName;
            }
            if (groupName) {
                query.groupName = groupName;
            }
            if (Name) {
                query.Name = Name;
            }

            const totalItemsCount = await languageModel.countDocuments(query);
            const totalPages = Math.ceil(totalItemsCount / size);
            const pageNumber = Math.max(1, Math.min(page, totalPages));
            const skip = (pageNumber - 1) * size;

            let data = await languageModel.find(query)
                .skip(skip)
                .limit(size)
                .sort({ createdAt: -1 })
                .exec();

            return {
                message: "Data fetched successfully",
                data: {
                    item: data
                },
                totalItemsCount,
                totalPages,
                page: pageNumber,
                size,
            };
        } catch (error) {
            console.error("Error fetching data:", error);
            throw error;
        }
    }

    async deleteDataById(groupId, langaugeId) {
        try {
            const updatedServicerequestData = await ServicerequestModel.deleteMany(
                {
                    groupId: groupId,
                    langaugeId: { $in: langaugeId },
                },
                { $set: { deleted: true } }
            );

            return updatedServicerequestData;
        } catch (error) {
            throw error;
        }
    }
    }
    

module.exports = new languageService(languageModel, "language");

