const WadiModel = require("../schema/wadi.schema");
const BaseService = require("@baapcompany/core-api/services/base.service");

class WadiService extends BaseService {
    constructor(dbModel, entityName) {
        super(dbModel, entityName);
    }
    getAllDataByGroupId(wadiId, criteria) {
        const query = {
            wadiId: wadiId,
        };
        if (criteria.name) query.name = new RegExp(criteria.name, "i");
        return this.preparePaginationAndReturnData(query, criteria);
    }
    async findByWadi(wadi) {
        return this.execute(() => {
            return this.model.findOne({ wadi: wadi });
        });
    }
    async deleteAllByCriteria(criteria) {
        try {
            // Use Mongoose's deleteMany to remove documents matching the criteria
            const deleteResult = await WadiModel.deleteMany(criteria).exec();

            if (deleteResult.deletedCount === 0) {
                return { message: "No wadis found to delete" };
            }
            return { message: "All LGD data deleted successfully" };
        } catch (error) {
            throw error;
        }
    }
    async getAllDataByWadiName(wadi, criteria) {
        const query = {
            wadi: wadi,
        };
        if (criteria.wadi) query.wadi = criteria.wadi;

        return this.preparePaginationAndReturnData(query, criteria);
    }
}
module.exports = new WadiService(WadiModel, "Wadi");
