const TokenModel = require("../schema/token.schema");
const LgdModel = require("../schema/token.schema");
const BaseService = require("@baapcompany/core-api/services/base.service");

class LgdService extends BaseService {
    constructor(dbModel, entityName) {
        super(dbModel, entityName);
    }
    getAllDataByGroupId(tokenId, criteria) {
        const query = {
            tokenId: tokenId,
        };

        if (criteria.name) query.name = new RegExp(criteria.name, "i");

        return this.preparePaginationAndReturnData(query, criteria);
    }
    async findByKey(key) {
        return this.execute(() => {
            return this.model.findOne({ key: key });
        });
    }
    async findByModule(module) {
        return this.execute(() => {
            return this.model.findOne({ module: module });
        });
    }
    async findByTag(tag) {
        return this.execute(() => {
            return this.model.findOne({ tag: tag });
        });
    }
    async deleteAllByCriteria(criteria) {
        try {
            // Use Mongoose's deleteMany to remove documents matching the criteria
            const deleteResult = await LgdModel.deleteMany(criteria).exec();

            if (deleteResult.deletedCount === 0) {
                return { message: "No tokens found to delete" };
            }
            return { message: "tokens deleted successfully" };
        } catch (error) {
            throw error;
        }
    }
    async getAllTokens() {
        return await LgdModel.find();
    }
    async findTokensByKey(key) {
        return await LgdModel.find({ key: key });
    }
    async getAllDataByKey(key, criteria) {
        const query = {
            key: key,
        };
        if (criteria.key) query.key = criteria.key;

        return this.preparePaginationAndReturnData(query, criteria);
    }
    
    async  getDataByGroupIdAndMemberId(groupId, appId, name, module, key, groupName, language, page, size) {
        try {
            let query = { groupId };
    
            if (appId) {
                query.appId = parseInt(appId);
            }
            if (module) {
                query.module = module;
            }
            if (key) {
                query.key = key;
            } 
            if (name) {
                query.name = name;
            }
            if (groupName) {
                query.groupName = groupName;
            }
            if (language) {
                query.language = language;
            }
    
            const totalItemsCount = await TokenModel.countDocuments(query);
    
            if (totalItemsCount === 0) {
                // If no matching data found, return an error
                return { error: "No data found for the given query parameters." };
            }
    
            const totalPages = Math.ceil(totalItemsCount / size);
            const pageNumber = Math.max(1, Math.min(page, totalPages));
            const skip = (pageNumber - 1) * size;
    
            let data = await TokenModel.find(query)
                .skip(skip)
                .limit(size)
                .sort({ createdAt: -1 })
                .exec();
    
            return {
                message: "Data fetched successfully",
                data: {
                   data
                },
                totalItemsCount,
                totalPages,
                page: pageNumber,
                size,
            };
        } catch (error) {
            console.error("Error fetching data:", error);
            throw error;
        }
    }
    
}
module.exports = new LgdService(LgdModel, "Lgd");
