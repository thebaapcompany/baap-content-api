const BaseService = require("@baapcompany/core-api/services/base.service");
const newsModel = require("../schema/news.schema");

class newsService extends BaseService {
    constructor(dbModel, entityName) {
        super(dbModel, entityName);
    }

    async findByCode(code) {
        return this.execute(() => {
            return this.model.findOne({ code: code });
        });
    }

    async findByAppId(appId) {
        return this.execute(() => {
            return this.model.findOne({ appId: appId });
        });
    }

    async findBygroupId(groupId) {
        return this.execute(() => {
            return this.model.findOne({ groupId: groupId });
        });
    }

    async selectByApp(appId) {
        return this.execute(() => {
            return this.model.findOne({ appId: appId });
        });
    }

    async getDataByGroupIdAndYear(
        groupId,
        userId,
        managerId,
        search,
        page,
        size,
        year,
        month,
        author
    ) {
        try {
            const query = { groupId };
            if (year) {
                const startOfYear = new Date(Date.UTC(year, 0, 1)); // January 1st, 00:00:00 UTC
                const endOfYear = new Date(
                    Date.UTC(year, 11, 31, 23, 59, 59, 999)
                ); // December 31st, 23:59:59 UTC

                query.date = {
                    $gte: startOfYear,
                    $lte: endOfYear,
                };
            }

            if (month) {
                const requestedMonth = month.trim();
                const upperYear = year
                    ? parseInt(year)
                    : new Date().getFullYear();
                const startOfMonth = new Date(
                    requestedMonth + " 1 " + upperYear
                );
                const endOfMonth = new Date(
                    new Date(requestedMonth + " 1 " + upperYear).setMonth(
                        startOfMonth.getMonth() + 1
                    ) - 1
                );

                query.date = {
                    $gte: startOfMonth.toISOString(),
                    $lte: endOfMonth.toISOString(),
                };
            }
            if (managerId) {
                query.managerId = parseInt(managerId);
            }
            if (userId) {
                query.userId = parseInt(userId);
            }
            if (search) {
                const numericSearch = parseInt(search);
                const searchRegex = { $regex: search, $options: "i" };

                query.$or = [
                    { title: searchRegex },
                    { description: searchRegex },
                    { appName: searchRegex },
                    { module: searchRegex },
                    { author: searchRegex },
                    { status: searchRegex },
                    { phoneNumber: numericSearch },
                    { managerId: numericSearch },
                    { assignedTo: searchRegex },
                ];
            }

            if (author) {
                query.author = { $regex: author, $options: "i" };
            }

            const totalItemsCount = await newsModel.countDocuments(query);
            const totalPages = Math.ceil(totalItemsCount / size);
            const pageNumber = Math.max(1, Math.min(page, totalPages));
            const skip = (pageNumber - 1) * size;

            const data = await newsModel
                .find(query)
                .skip(skip)
                .limit(size)
                .sort({ createdAt: -1 })
                .exec();

            return {
                message: "Data fetched successfully",
                data,
                totalItemsCount,
                totalPages,
                page: pageNumber,
                size,
            };
        } catch (error) {
            console.error("Error fetching data:", error);
            throw error;
        }
    }
}

module.exports = new newsService(newsModel, "news");
