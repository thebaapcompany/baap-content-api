const LgdModel = require("../schema/lgd.schema");
const BaseService = require("@baapcompany/core-api/services/base.service");
class LgdService extends BaseService {
    constructor(dbModel, entityName) {
        super(dbModel, entityName);
    }
    getAllDataByGroupId(lgdId, criteria) {
        const query = {
            lgdId: lgdId,
        };
        if (criteria.name) query.name = new RegExp(criteria.name, "i");
        return this.preparePaginationAndReturnData(query, criteria);
    }
    async findByLgd(lgd) {
        return this.execute(() => {
            return this.model.findOne({ lgd: lgd });
        });
    }
    async deleteAllByCriteria(criteria) {
        try {
            // Use Mongoose's deleteMany to remove documents matching the criteria
            const deleteResult = await LgdModel.deleteMany(criteria).exec();

            if (deleteResult.deletedCount === 0) {
                return { message: "No lgds found to delete" };
            }
            return { message: "All LGD data deleted successfully" };
        } catch (error) {
            throw error;
        }
    }
    async getAllDataByLgd(lgd, criteria) {
        const query = {
            lgd: lgd,
        };
        if (criteria.lgd) query.lgd = criteria.lgd;

        return this.preparePaginationAndReturnData(query, criteria);
    }
}
module.exports = new LgdService(LgdModel, "Lgd");
