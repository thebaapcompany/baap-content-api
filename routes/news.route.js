const express = require("express");
const router = express.Router();
const { checkSchema } = require("express-validator");
const service = require("../services/news.service");
const requestResponsehelper = require("@baapcompany/core-api/helpers/requestResponse.helper");
const ValidationHelper = require("@baapcompany/core-api/helpers/validation.helper");
const { ChainCondition } = require("express-validator/src/context-items");

router.post(
    "/",
    checkSchema(require("../dto/news.dto")),
    async (req, res, next) => {
        if (ValidationHelper.requestValidationErrors(req, res)) {
            return;
        }
        const data = req.body.groupId;
        const serviceResponse = await service.create(req.body);
        requestResponsehelper.sendResponse(res, serviceResponse);
    }
);

router.get("/all/news", async (req, res) => {
    const serviceResponse = await service.getAllByCriteria({});

    requestResponsehelper.sendResponse(res, serviceResponse);
});
router.get("/findById/:id", async (req, res) => {
    const id = req.params.id;
    const serviceResponse = await service.findByCode(id); // Call the findbycode function

    requestResponsehelper.sendResponse(res, serviceResponse);
});
router.get("/getDataByAppId/:appId", async (req, res) => {
    const appId = req.params.appId;
    const serviceResponse = await service.findByAppId(appId);

    requestResponsehelper.sendResponse(res, serviceResponse);
});
router.get("/getDataBygroupId/:groupId", async (req, res) => {
    const groupId = req.params.groupId;
    const serviceResponse = await service.findBygroupId(groupId);

    requestResponsehelper.sendResponse(res, serviceResponse);
});

router.put("/:id", async (req, res) => {
    const serviceResponse = await service.updateById(req.params.id, req.body);
    requestResponsehelper.sendResponse(res, serviceResponse);
});
router.delete("/:id", async (req, res) => {
    const serviceResponse = await service.deleteById(req.params.id);
    requestResponsehelper.sendResponse(res, serviceResponse);
});

router.post(
    "/ToolBox",
    checkSchema(require("../dto/news.dto")),
    async (req, res, next) => {
        if (ValidationHelper.requestValidationErrors(req, res)) {
            return;
        }

        const serviceResponse = await service.create(req.body);
        requestResponsehelper.sendResponse(res, serviceResponse);
    }
);

router.get("/groupId/:groupId", async (req, res) => {
    const { groupId } = req.params;
    const page = parseInt(req.query.page) || 1;
    const size = parseInt(req.query.size) || 10;
    const year = req.query.year ? parseInt(req.query.year) : null;
    const month = req.query.month;
    const author = req.query.author;
    const managerId = req.query.managerId;
    const userId = req.query.userId;
    const search = req.query.search;

    try {
        const serviceResponse = await service.getDataByGroupIdAndYear(
            groupId,
            userId,
            managerId,
            search,
            page,
            size,
            year,
            month,
            author
        );

        res.status(200).json({
            message: "Data Fetched Successfully",
            data: serviceResponse.data,
            TotalItemsCount: serviceResponse.totalItemsCount,
            page: serviceResponse.page,
            size: serviceResponse.size,
        });
    } catch (error) {
        console.error("Error Occurred:", error);
    }
});

module.exports = router;
