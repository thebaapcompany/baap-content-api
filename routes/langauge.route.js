const express = require("express");
const router = express.Router();
const { checkSchema } = require("express-validator");

const service = require("../services/language.service");
const requestResponsehelper = require("@baapcompany/core-api/helpers/requestResponse.helper");
const ValidationHelper = require("@baapcompany/core-api/helpers/validation.helper");
const { ChainCondition } = require("express-validator/src/context-items");

router.post(
    "/",
    checkSchema(require("../dto/language.dto")),
    async (req, res, next) => {
        if (ValidationHelper.requestValidationErrors(req, res)) {
            return;
        }
        const langaugeId = +Date.now();
    req.body.langaugeId = langaugeId;
        const serviceResponse = await service.create(req.body);
        requestResponsehelper.sendResponse(res, serviceResponse);
    }
);

//get by code
router.get("/getDataByCode/:code", async (req, res) => {
    const code = req.params.code;

    
    const serviceResponse = await service.findByCode(code); // Call the findbycode function

    requestResponsehelper.sendResponse(res, serviceResponse);
});


router.get("/all/language", async (req, res) => {
    const serviceResponse = await service.getAllByCriteria({});

    requestResponsehelper.sendResponse(res, serviceResponse);
});
router.get("/findById/:id", async (req, res) => {
    const id = req.params.id;
    const serviceResponse = await service.findByCode(id); // Call the findbycode function

    requestResponsehelper.sendResponse(res, serviceResponse);
});
router.get("/getDataByName/:name", async (req, res) => {
    const name = req.params.name;
    const serviceResponse = await service.findByName(name); // Call the findbycode function

    requestResponsehelper.sendResponse(res, serviceResponse);
});


router.put("/:id", async (req, res) => {
    const serviceResponse = await service.updateById(req.params.id, req.body);
    requestResponsehelper.sendResponse(res, serviceResponse);
});

router.delete("/groupId/:groupId", async (req, res) => {
    const groupId = req.params.groupId;
    const langaugeIds = req.query.langaugeId;

    if (!langaugeIds) {
        return res.status(400).json({ message: "langaugeIds parameter is missing" });
    }

    const langaugeId = langaugeIds.split(',');

    if (langaugeId.length === 0) {
        return res.status(400).json({ message: "langaugeIds parameter is empty" });
    }

    try {
        let deletedServicerequestData = await service.deleteDataById(groupId, langaugeId);
        res.status(200).json({
            message: "Data delete successfully",
            data: deletedServicerequestData,
        });
    } catch (error) {
        res.status(500).json({ message: "An error occurred while deleting data" });
    }
});
router.delete("/:id", async (req, res) => {
    const serviceResponse = await service.deleteById(req.params.id);

    requestResponsehelper.sendResponse(res, serviceResponse);
});

router.delete("/groupId/:groupId", async (req, res) => {
    const groupId = req.params.groupId;
    const langaugeIds = req.query.langaugeId;

    if (!langaugeIds) {
        return res.status(400).json({ message: "langaugeIds parameter is missing" });
    }

    const langaugeId = langaugeIds.split(',');

    if (langaugeId.length === 0) {
        return res.status(400).json({ message: "langaugeIds parameter is empty" });
    }

    try {
        let deletedServicerequestData = await service.deleteDataById(groupId, langaugeId);
        res.status(200).json({
            message: "Data delete successfully",
            data: deletedServicerequestData,
        });
    } catch (error) {
        res.status(500).json({ message: "An error occurred while deleting data" });
    }
});
router.get("/all/getByGroupId/:groupId", async (req, res) => {
    const groupId = req.params.groupId;
    const criteria = {
      
     
        pageNumber: parseInt(req.query.pageNumber) || 1,
        pageSize: parseInt(req.query.pageSize) || 10, //
    };

    const serviceResponse = await service.getAllDataByGroupId(
        groupId,
        criteria
    );
    requestResponsehelper.sendResponse(res, serviceResponse);
});
router.get('/groupId/:groupId', async (req, res) => {
    if (ValidationHelper.requestValidationErrors(req, res)) {
        return;
    }
    const page = parseInt(req.query.page) || 10;
    const size = parseInt(req.query.size);
    const appId = req.query.appId;
    const moduleName = req.query.moduleName;
    const key = req.query.key;
    const appName = req.query.appName;
    const groupName = req.query.groupName;
    const Name = req.query.Name;

    const { groupId } = req.params;

    try {
        const serviceResponse = await service.getDataByGroupIdAndMemberId(groupId, appId, appName, moduleName, Name, key, groupName, Name, page, size);
        const data = serviceResponse.data;
        res.status(200).json({
            message: "Data fetched successfully",
            data,
            totalItemsCount: serviceResponse.totalItemsCount,
            page,
            size,
        });
    } catch (error) {
        console.error("Error occurred:", error);
        res.status(500).json({ message: "Internal server error" });
    }
});

module.exports = router;

