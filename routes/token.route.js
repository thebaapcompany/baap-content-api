const express = require("express");
const router = express.Router();
const { checkSchema } = require("express-validator");
const service = require("../services/token.service");
const requestResponsehelper = require("@baapcompany/core-api/helpers/requestResponse.helper");
const ValidationHelper = require("@baapcompany/core-api/helpers/validation.helper");

router.post(
    "/createToken",
    checkSchema(require("../dto/token.dto")),
    async (req, res, next) => {
        if (ValidationHelper.requestValidationErrors(req, res)) {
            return;
        }
        const serviceResponse = await service.create(req.body);
        requestResponsehelper.sendResponse(res, serviceResponse);
    }
);
router.get("/all/tokens", async (req, res) => {
    try {
        const allTokens = await service.getAllTokens();
        res.json({ status: "Success", data: allTokens });
    } catch (error) {
        res.status(500).json({ status: "Error", message: error.message });
    }
});
router.get("/tokens/search", async (req, res) => {
    const keyToFind = req.query.key;

    try {
        if (!keyToFind) {
            return res.status(400).json({ status: "Failed", message: "Key parameter is required" });
        }

        const tokensWithKey = await service.findTokensByKey(keyToFind);
        res.json({ status: "Success", data: tokensWithKey });
    } catch (error) {
        res.status(500).json({ status: "Error", message: error.message });
    }
});
router.delete("/DeleteByTokenId/:id", async (req, res) => {
    const serviceResponse = await service.deleteById(req.params.id);
    requestResponsehelper.sendResponse(res, serviceResponse);
});
router.put("/updateByTokenId/:id", async (req, res) => {
    const serviceResponse = await service.updateById(req.params.id, req.body);
    requestResponsehelper.sendResponse(res, serviceResponse);
});
router.get("/findByTokenId/:id", async (req, res) => {
    const serviceResponse = await service.getById(req.params.id ,req.body);
    requestResponsehelper.sendResponse(res, serviceResponse);
});
router.get("/getDataByKey/:key", async (req, res) => {
    const key = req.params.key;
    const serviceResponse = await service.findByKey(key);
    requestResponsehelper.sendResponse(res, serviceResponse);
});
router.get("/getDataByModule/:module", async (req, res) => {
    const module = req.params.module;
    const serviceResponse = await service.findByModule(module);
    requestResponsehelper.sendResponse(res, serviceResponse);
});
router.get("/getDataByTag/:tag", async (req, res) => {
    const tag = req.params.tag;
    const serviceResponse = await service.findByTag(tag);
    requestResponsehelper.sendResponse(res, serviceResponse);
});
router.delete("/all/tokens", async (req, res) => {
    const serviceResponse = await service.deleteAllByCriteria({});
    requestResponsehelper.sendResponse(res, serviceResponse);
});
router.get("/all/getBykey/:key", async (req, res) => {
    const key = req.params.key;
    const criteria = {
      
        key: req.query.key,
        pageNumber: parseInt(req.query.pageNumber) || 1,
        pageSize: parseInt(req.query.pageSize) || 10,
    };

    const serviceResponse = await service.getAllDataByKey(
        key,
        criteria
    );
    requestResponsehelper.sendResponse(res, serviceResponse);
});
router.get("/searchToken/:schema/:value", async (req, res) => {
    const schema = req.params.schema;
    const value = req.params.value;

    try {
        const serviceResponse = await service.searchTokenByValue(schema, value);
        requestResponsehelper.sendResponse(res, serviceResponse);
    } catch (error) {
        // Handle invalid schema error
        requestResponsehelper.sendResponse(res, { status: "Failed", message: error.message });
    }
});
router.get('/groupId/:groupId', async (req, res) => {
    const { groupId } = req.params;
    const page = parseInt(req.query.page) || 1;
    const size = parseInt(req.query.size) || 10; // Set a default size if not provided
    const appId = req.query.appId;
    const module = req.query.module;
    const key = req.query.key;
    const name = req.query.name;
    const groupName = req.query.groupName;
    const language = req.query.language;

    try {
        // Validate request parameters
        if (!groupId) {
            return res.status(400).json({ message: "groupId parameter is required" });
        }
        
        // Call the service function to fetch data
        const serviceResponse = await service.getDataByGroupIdAndMemberId(groupId, appId, name, module, key, groupName, language, page, size);
        
        // Check if any error occurred in the service function
        if (serviceResponse.error) {
            return res.status(400).json({ message: serviceResponse.error });
        }

        // If no errors, return the data
        const data = serviceResponse.data;
        res.status(200).json({
            message: "Data fetched successfully",
            data,
            totalItemsCount: serviceResponse.totalItemsCount,
            page,
            size,
        });
    } catch (error) {
        console.error("Error occurred:", error);
        res.status(500).json({ message: "Internal server error" });
    }
});


module.exports = router;
