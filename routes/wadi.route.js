const express = require("express");
const router = express.Router();
const { checkSchema } = require("express-validator");
const service = require("../services/wadi.service");
const requestResponsehelper = require("@baapcompany/core-api/helpers/requestResponse.helper");
const ValidationHelper = require("@baapcompany/core-api/helpers/validation.helper");

router.post(
    "/create",
    checkSchema(require("../dto/wadi.dto")),
    async (req, res, next) => {
        if (ValidationHelper.requestValidationErrors(req, res)) {
            return;
        }
        const serviceResponse = await service.create(req.body);
        requestResponsehelper.sendResponse(res, serviceResponse);
    }
);
router.get("/all/wadi", async (req, res) => {
    const serviceResponse = await service.getAllByCriteria({});
    requestResponsehelper.sendResponse(res, serviceResponse);
});
router.delete("/DeleteByWadiId/:id", async (req, res) => {
    const serviceResponse = await service.deleteById(req.params.id);
    requestResponsehelper.sendResponse(res, serviceResponse);
});
router.put("/updateByWadiId/:id", async (req, res) => {
    const serviceResponse = await service.updateById(req.params.id, req.body);
    requestResponsehelper.sendResponse(res, serviceResponse);
});
router.get("/findByWadiId/:id", async (req, res) => {
    const serviceResponse = await service.getById(req.params.id);
    requestResponsehelper.sendResponse(res, serviceResponse);
});
router.get("/getBywadiNo/:wadi", async (req, res) => {
    const wadi = req.params.wadi;
    const serviceResponse = await service.findByWadi(wadi);
    requestResponsehelper.sendResponse(res, serviceResponse);
});
router.delete("/deleteAllRecords", async (req, res) => {
    const serviceResponse = await service.deleteAllByCriteria({});
    requestResponsehelper.sendResponse(res, serviceResponse);
});
router.get("/all/getBywadi/:wadi", async (req, res) => {
    const wadi = req.params.key;
    const criteria = {
        wadi: req.query.wadi,
    };
    const serviceResponse = await service.getAllDataByWadiName(
        wadi,
        criteria
    );
    requestResponsehelper.sendResponse(res, serviceResponse);
});
module.exports = router;
