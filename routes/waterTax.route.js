const express = require("express");
const router = express.Router();
const { checkSchema } = require("express-validator");
const service = require("../services/waterTax.service");
const requestResponsehelper = require("@baapcompany/core-api/helpers/requestResponse.helper");
const ValidationHelper = require("@baapcompany/core-api/helpers/validation.helper");
router.post(
    "/createTax",
    checkSchema(require("../dto/waterTax.dto")),
    async (req, res, next) => {
        if (ValidationHelper.requestValidationErrors(req, res)) {
            return;
        }
        const serviceResponse = await service.create(req.body);
        requestResponsehelper.sendResponse(res, serviceResponse);
    }
);
router.get("/all/user", async (req, res) => {
    const serviceResponse = await service.getAllByCriteria({});
    requestResponsehelper.sendResponse(res, serviceResponse);
});
router.delete("/DeleteBywaterTaxId/:id", async (req, res) => {
    const serviceResponse = await service.deleteById(req.params.id);
    requestResponsehelper.sendResponse(res, serviceResponse);
});
router.put("/updateBywaterTaxId/:id", async (req, res) => {
    const serviceResponse = await service.updateById(req.params.id, req.body);
    requestResponsehelper.sendResponse(res, serviceResponse);
});
router.get("/findBywaterTaxId/:id", async (req, res) => {
    const serviceResponse = await service.getById(req.params.id);
    requestResponsehelper.sendResponse(res, serviceResponse);
});
router.get("/getBytaxationPeriod/:taxationPeriod", async (req, res) => {
    const  taxationPeriod = req.params.taxationPeriod;
    const serviceResponse = await service.findByLgd(taxationPeriod);
    requestResponsehelper.sendResponse(res, serviceResponse);
});
router.delete("/deleteAllRecords", async (req, res) => {
    const serviceResponse = await service.deleteAllByCriteria({});
    requestResponsehelper.sendResponse(res, serviceResponse);
});
router.get("/all/getByTaxationPeriod/:taxationPeriod", async (req, res) => {
    const taxationPeriod = req.params.taxationPeriod;
    const criteria = {
        taxationPeriod: req.query.taxationPeriod,
    };
    const serviceResponse = await service.getAllDataByTaxationPeriod(
        taxationPeriod,
        criteria
    );
    requestResponsehelper.sendResponse(res, serviceResponse);
});
module.exports = router;
