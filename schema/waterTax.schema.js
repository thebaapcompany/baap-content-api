const mongoose = require("mongoose");
const waterTaxSchema = new mongoose.Schema(
    {
        propertyArea: [{
            min: {
                type: Number,
                require: false
            },
            max: {
                type: Number,
                require: false
            }
        }],
        tax: [{
            min: {
                type: Number,
                require: false
            },
            max: {
                type: Number,
                require: false
            }
        }],
        taxationPeriod: {
            type: String,
            require: false
        }
    }
)
const waterTaxModel = mongoose.model("watertax", waterTaxSchema);
module.exports = waterTaxModel;