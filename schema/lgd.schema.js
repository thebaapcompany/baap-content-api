const mongoose = require("mongoose");
const LgdSchema = new mongoose.Schema(
    {
        lgd: {
            type: Number,
            require: false
        },
        Name: {
            type: String,
            require: false,
        },
        pincode: {
            type: Number,
            require: false
        },
        address: {
            type: String,
            require: false
        }
    },
    { strict: false, timestamps: true }
);
const LgdModel = mongoose.model("Lgd", LgdSchema);
module.exports = LgdModel;


