const mongoose = require("mongoose");
const newsSchema = new mongoose.Schema(
    {
        groupId: [
            {
                type: Number,
            },
        ],
        title: {
            type: String,
            required: true,
        },
        description: {
            type: String,
        },
        appName: {
            type: String,
        },
        module: {
            type: String,
        },
        author: {
            type: String,
        },
        status: {
            type: String,
        },
        date: {
            type: Date,
        },
    },
    { strict: false }
);
const newsModel = mongoose.model("news", newsSchema);
module.exports = newsModel;
