const mongoose = require("mongoose");

const languageSchema = new mongoose.Schema(
    {
        name: {
            type: String,
        },
        groupId: {
            type: Number,
        },
        description: {
            type: String,
        },
        appId: {
            type: Number,
        },
        icon: {
            type: String,
        },
        langaugeId: {
            type: Number,
        },
        appName: {
            type: String,
            require: false,
        },
        groupName: {
            type: String,
            require: false,
        },
        moduleName: {
            type: String,
            require: false,
        },
    },
    { strict: false }
);
const languageModel = mongoose.model("language", languageSchema);
module.exports = languageModel;
