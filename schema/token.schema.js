const mongoose = require("mongoose");
const TokenSchema = new mongoose.Schema(
    {
        userId: {
            type: mongoose.Schema.Types.ObjectId,
           
        },
        appId: {
            type: Number,
           
        },
        groupId: {
            type: Number,
            
        },
        language: {
            
                type: String,
               
            },
        
        name: {
            type: String,
            
        },
        groupName: {
            type: String,
           
        },
        module: {
            type: String,
            
        },
        tags: {
            type: String,
           
        },
        key: {
            type: String,
           
        }
    },
    { strict: true, }
);
const TokenModel = mongoose.model("Token", TokenSchema);
module.exports = TokenModel;


