const mongoose = require("mongoose");
const WadiSchema = new mongoose.Schema(
    {
        gpId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "GramPanchayath",
            autopopulate: true,
        },
        villageName: {
            type: String,
            required: false,
        },
        wadiName: {
            type: String,
            required: false,
        },
    },
    { strict:false,timestamps: true }
);
WadiSchema.plugin(require("mongoose-autopopulate"));
const WadiModel = mongoose.model("wadi", WadiSchema);
module.exports = WadiModel;
